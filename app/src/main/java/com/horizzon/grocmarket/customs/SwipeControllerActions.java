package com.horizzon.grocmarket.customs;

public abstract class SwipeControllerActions {
    public void onLeftClicked(int position) {}

    public void onRightClicked(int position) {}
}
