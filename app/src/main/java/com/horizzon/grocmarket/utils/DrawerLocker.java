package com.horizzon.grocmarket.utils;

public interface DrawerLocker {
    void setDrawerEnabled(boolean enabled);
}
